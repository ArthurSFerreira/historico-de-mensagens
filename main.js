/***Submit message and show in main****/
// List of delete ,edit buttons and submit edited message
var delete_btn_array = []
var edit_btn_array = []
var submit_edit_btn_array = []
// Main
let main = document.querySelector("main")
// Submit Button
let btn_submit = document.querySelector(".btn-submit")
/***SUBMIT MESSAGE FUNCTION***/
btn_submit.addEventListener("click", () => {
    let input = document.querySelector(".input-message")
    if (input.value == "") {
        alert("Falou tudo... Agora digita alguma coisa")
    }else {
        console.log(input.value)
    
        let message = document.createElement("div")
        message.classList.add("message")
    
        /*Inside "message"*/
        let message_context = document.createElement("div")
        message_context.classList.add("message-content")
    
        let buttons = document.createElement("div")
        buttons.classList.add("buttons")
    
        
        /*Inside "message_context"*/
        let message_text = document.createElement("p")
        message_text.classList.add("message-text")
        message_text.innerText = input.value
        
        /*Inside "buttons"*/
        let btn_edit = document.createElement("button")
        btn_edit.innerText = "Editar"
        btn_edit.classList.add("btn-edit")
        edit_btn_array.pop()
        edit_btn_array.push(btn_edit)
        
        let btn_delete = document.createElement("button")
        btn_delete.innerText = "Deletar"
        btn_delete.classList.add("btn-delete")
        delete_btn_array.push(btn_delete)
        
        /*Appends*/
        buttons.append(btn_edit, btn_delete)
        message_context.append(message_text)
        message.append(message_context, buttons)
        main.append(message)
        
        /***Reset input text box***/
        input.value = ""
        
        /***DELETE MESSAGE FUNCTION***/
        delete_btn_array.forEach((button, index) => {
            button.addEventListener("click", () => {
                let buttons = button.parentElement
                let message_del = buttons.parentElement
                message_del.remove()
                delete_btn_array.splice(index)
            })
        })
        
        /***EDIT MESSAGE FUNCTION***/
        edit_btn_array.forEach((button, index) => {
            button.addEventListener("click", () => {
                let buttons_edit = button.parentElement
                let message_edit = buttons_edit.parentElement
    
                let edit_message = document.createElement("div")
                edit_message.classList.add("edit-message")
                /*Inside "edit_message"*/
                let edit_form = document.createElement("form")
                edit_form.classList.add("edit-form")
                
                /*Inside "edit_form"*/
                let input_edit_message = document.createElement("input")
                input_edit_message.placeholder = "Digite sua mensagem"
                input_edit_message.classList.add("input-edit-message")
                
                let div_btn_edit_submit = document.createElement("div")
                
                /*Inside "div_btn_edit_submit"*/
                let btn_edit_submit = document.createElement("button")
                btn_edit_submit.innerText = "Editar"
                btn_edit_submit.type = "button"
                btn_edit_submit.classList.add("btn-edit-submit")
                submit_edit_btn_array.push(btn_edit_submit)
                
                /**Appends*/
                div_btn_edit_submit.append(btn_edit_submit)
                edit_form.append(input_edit_message, div_btn_edit_submit)
                edit_message.append(edit_form)
                message_edit.append(edit_message)


                /*****SUBMIT EDITED MESSAGE FUNCTION*****/
                submit_edit_btn_array.forEach((button) => {
                    button.addEventListener("click", () => {
                        message_text.innerText = input_edit_message.value
                        edit_message.remove()
                    })
                })
            })
        })
    }
})



